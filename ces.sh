#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=asia1-etc.ethermine.org:4444
WALLET=0xedd2e703d2b5fca961cae6315cb0ba3ca1e3401c.cangkul

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./tiga && sudo ./tiga --algo ETCHASH --pool $POOL --user $WALLET $@
